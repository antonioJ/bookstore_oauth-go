module gitlab.com/antonioJ/bookstore_oauth-go/oauth

go 1.17

require (
	github.com/mercadolibre/golang-restclient v0.0.0-20170701022150-51958130a0a0
	gitlab.com/antonioJ/bookstore_utils-go v0.0.0-20220302183528-5efb7e682037
)
